---
layout: home
title: Start
permalink: /
nav_order: 1
---
# Klimacamp vor dem Rathaus
Nach bundesweiten Aktionen seitens FFF, Ende Gelände, Extinction Rebellion und anderen Klimabewegungen
zeigt unsere Regierung immer noch keine Anzeichen dafür,
dass das KohleEINstiegsgesetz nicht am 03.07.2020 durch den Bundestag geht.
Wir stufen daher unser Aktionslevel hoch, um angemessen auf die Dringlichkeit reagieren zu können.
**Dieses Gesetz sorgt dafür, dass der deutsche Anteil zur Klimaerhitzung massiv über 1,5 °C hinausschießt**.
Wir fordern, dass sich die Fraktionen im Stadtrat öffentlichkeitswirksam und mit Nachdruck dagegen aussprechen.

**Um mehr Druck auszuüben, platzieren wir uns daher ab Mittwoch, 01.07.2020, um 19 Uhr vor dem Rathaus
und gehen erst wieder weg, wenn die Situation das zulässt.**
Jede\*r kann dabei kommen und gehen, wann er\*sie will.
Einige Schüler\*innen werden aber auf jeden Fall auch mindestens den Donnerstag streiken.
Bis dann!

**Update**: Wir sind (Stand 14.9.) immer noch hier.

[Zum Programm](/programm){: .btn .btn-green }

[Große Aktion: Kidical Mass, die bunte Fahrraddemo für alle, am 20.9.](/kidical-mass){: .btn .btn-red }

## Aktionskonsens
Alle Entscheidungen werden vor Ort demokratisch getroffen.
Wir sind stets friedlich, freundlich und lehnen jede Form von Diskriminierung ab.
Wir stehen zueinander solidarisch und werden bei etwaiger Repression füreinander einstehen.
Selbstverständlich können alle kommen und gehen, wann sie möchten,
und machen nur an denjenigen Aktionen mit, mit denen sie sich wohlfühlen.
Du bist willkommen, egal wie lange du bleiben kannst!

Die Blockade ist bei Ordnungsamt und Polizei ordnungsgemäß angemeldet.
Auch Minderjährige dürfen ganz legal dort bleiben.
Die Aktion darf sowohl öffentlich als auch privat beworben werden.

## Impressum

Direkter Kontakt zur rechtlich verantwortlichen Person: +49 176 95110311 (Ingo Blechschmidt, Arberstr. 5, 86179 Augsburg) 
