---
layout: page
title: Kidical Mass
permalink: /kidical-mass/
nav_order: 8
---

# Kidical Mass, die bunte Fahrraddemo für alle!

![Kidical Mass am 20.9.2020 in Augsburg](/pages/Pressemitteilungen/Kidical-Mass.jpeg)

Am 20. September erobern große und kleine Radfahrende die Straßen von Augsburg,
bei der erste Augsburger Kidical Mass, einer bunten Fahrrad-Demo für alle.
Warum? Weil auch Kinder gerne Radfahren! Deshalb haben wir eine „kinderleichte“
Route ausgearbeitet, die auch für die Kleinsten geeignet ist. Sie ist als
offizielle Demonstration angemeldet, die Augsburger Polizei begleitet uns und
sichert die Wege.

Die Tour beginnt um 14:00 Uhr am Königsplatz und wird am Wittelsbacher Park enden, wo
viel Platz zum Spielen oder einen anschließenden Familiennachmittag ist.
Eltern, Großeltern, die ganze Familie, Freund\*innen und alle, denen Kinder und
Fahrradfahren am Herzen liegen, können mitgebracht werden.

Übrigens: Am 20. September finden [bundesweit](https://www.kinderaufsrad.org/)
Kidical-Mass-Demos in mehr als 90 Orten statt – alle mit dem Ziel klarzumachen,
dass sich alle Kinder und Jugendlichen sicher und selbständig mit dem Fahrrad
in der Stadt bewegen können sollen.

Seid dabei, die Dinge ins Rollen zu bringen! #KidicalMass #MobilitätFürMenschen #KinderAufsRad #FahrradstadtJetzt

Um Infektionen vorzubeugen, ist Maske Pflicht. Außerdem sollen die einzelnen
mitfahrenden Haushalte deutlichen Abstand zwischeneinander halten. Zu Beginn
und am Ende werden wir reichlich Desinfektionsmittel bereitstellen. Zudem
werden wir, insofern wir sie besorgen können, Poolnudeln als Unterstützung zur
Abstandswahrung bereitstellen.
