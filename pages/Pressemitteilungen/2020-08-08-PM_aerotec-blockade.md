---
layout: page
title:  "08.08.2020: Keine Krisen befeuern — Aktivist*innen des Klimacamps blockieren friedlich Premium Aerotec"
date:   2020-08-08 07:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 8
---

*Pressemitteilung vom Augsburger Klimacamp am 8. August 2020*

# Keine Krisen befeuern — Aktivist\*innen des Klimacamps blockieren friedlich Premium Aerotec

Anlässlich des Augsburger Friedensfest blockierten heute morgen etwa 20 Aktivist\*innen des Augsburger Klimacamps friedlich die Zufahrt zum militärischen Werk von Premium Aerotec. Das passierte unter dem Motto: Keine Krisen befeuern!

In Augsburg produzierte Premium Aerotec bisher knapp 600 Rumpfmittelteile für Eurofighter [1], was die „Friedensstadt“ zu einem zentralen Teil der Produktionskette macht. Kampfflugzeuge im Wert von mehreren Milliarden Euro wurden unter anderem nach Saudi-Arabien geliefert, das sie seit vielen Jahren im Jemenkrieg, der bereits über hunderttausend Tote forderte [2], für Bombenangriffe einsetzt [3]. Dort werden mit Eurofightern auch schreckliche Kriegsverbrechen begangen [4]. Unsere Stadt feiert ein Friedensfest und finanziert das mit Krieg, der durch Rüstung exportiert wird.

„Klimagerechtigkeit ist unvereinbar mit der Unterstützung von Kriegen durch Rüstung“, erklärt Sarah Bauer (16) vom Camp den Grund für ihr Engagement. „Viele Kriege werden um die Verteilung und Ausbeutung von Wasser und Rohstoffen geführt. Solche Ressourcenkonflikte werden von der Klimakatastrophe zugespitzt und vervielfältigt, und betreffen hauptsächlich Menschen, die nicht für die Katastrophe verantwortlich sind.“

Waffengebrauch hat langfristige Konsequenzen. Ökosysteme vor Ort werden zerstört, was überlebende Menschen weiter in Armut und Abhängigkeit treibt. Klimagerechtigkeit fordert, dass alle Menschen gerecht diese Krise überstehen und überleben können und nicht die Unschuldigen die Verantwortung für diese Krise tragen müssen. Wir möchten Bewusstsein dafür schaffen, welche Rolle Augsburg in der Rüstung von aktiven Kriegen spielt und dass das nicht mit einer klimagerechten Friedenstadt vereinbar ist. Wir fordern, dass Augsburg transparenter über seine Rüstungsexporte ist und diese nicht mit einem vermeintlichen Friedensfest deckt.

[1] https://www.flugrevue.de/militaer/premium-aerotec-neue-eurofighter-teile-aus-augsburg/
[2] https://www.theguardian.com/world/2019/oct/31/death-toll-in-yemen-war-reaches-100000
[3] https://www.telegraph.co.uk/news/uknews/defence/11584269/Saudis-UK-made-war-jets-outnumber-RAFs.html
[4] https://www.hrw.org/report/2015/11/26/what-military-target-was-my-brothers-house/unlawful-coalition-airstrikes-yemen
