---
layout: page
title:  "28.07.2020: Wir streiken bis ihr handelt – Klimarat nimmt diesen Spruch wörtlich."
date:   2020-07-28 07:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 4
---

*Pressemitteilung vom Augsburger Klimacamp am 28. Juli 2020*

# Wir streiken bis ihr handelt – Klimarat nimmt diesen Spruch wörtlich.

Der Augsburger Klimarat zeigt sich präsent: Seit 28 Tagen veranstalten
Aktivist*innen ein Protestcamp neben dem Rathaus. Die Aktivist*innen äußern als
Anlass, dass die aktuelle Klimapolitik aus wissenschaftlicher Sicht
unzureichend ist. Ein Ende der Aktion ist bisher noch nicht abzusehen.

Das Bündnis aus Aktivist*innen diverser Klimagerechtigkeitsgruppen demonstriert
seit dem 1. Juli in einem durchgängigen Protestcamp. Dabei rufen sie die Stadt
Augsburg zu einer klaren Positionierung gegen das Kohleausstiegsgesetz der
Bundesregierung auf. Außerdem soll der Stadtrat sich für die Forderungen des
Augsburger Radentscheids einsetzen und das Einhalten des 1,5-°C-Ziels auf
kommunaler Ebene zu einem Ziel höchster Priorität erklären.

Die Kundgebungen der Augsburger Klimabewegungen sorgten dabei bundesweit für
Aufsehen: Eine Vielzahl von Besucher*innen aus den verschiedensten Teilen
Deutschlands zeigte sich von der Protestform inspiriert. Die Städte Nürnberg
und München starten bereits eigene Klimacamps, weitere Städte kündigten an zu
folgen.

Die Aktivist*innen zeigen sich entrüstet gegenüber des unzureichenden Einsatzes
der Stadtregierung im Klimaschutz: „Es kann nicht sein, dass der Stadtrat
wichtige Entscheidungen trotz der drängenden Situation immer wieder vertagt
oder vernachlässigt.“, so die 16-jährige Schülerin Sarah Bauer. „Der Antrag zur
dezentralen Energiewende wurde auf einen Zeitpunkt nach der Sommerpause
verlegt. Damit verschiebt der Stadtrat den dringend notwendigen Wandel und
provoziert damit einen Bruch mit feststehenden Klimazielen.“

Leon Ueberall, ebenfalls Aktivist auf dem Camp, ergänzt: „Wir streiken nun seit
über eineinhalb Jahren auf der Straße und campieren durchgängig seit 28 Tagen
neben dem Rathaus. Trotzdem bekommen wir nichts außer schwacher
Lippenbekenntnisse und scheinheiligem Zuspruch. Wir werden hier bleiben solange
es nötig ist. Die Klimakrise lässt dabei leider keine Kompromisse zu. Wir
benötigen echte, ernst gemeinte Maßnahmen zu Klimagerechtigkeit.“

„Ich kann nur staunen wie viel Motivation und Überzeugung trotz des Wetters,
des harten Bodens und der frustrierenden Politik hier herrscht. Das Augsburger
Klimacamp ein gutes Beispiel um zu zeigen, dass wir trotz allem aktiv bleiben“,
sagte Aktivistin Gwendolyn Rautenberg, die extra aus Brandenburg zum Klimacamp
anreiste. Von nun an zeige man sich hartnäckiger: Ab jetzt würden freitags
wieder wöchentliche Demonstrationen stattfinden, die erste davon sei am 31.07.
um 17 Uhr. Startpunkt würde wie immer das Klimacamp sein.
