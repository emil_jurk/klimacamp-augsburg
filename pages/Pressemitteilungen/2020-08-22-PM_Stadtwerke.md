---
layout: page
title:  "22.08.2020: Klimagerechtigkeitsaktivist*innen übernehmen Marketingjob der Stadtwerke"
date:   2020-08-22 07:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 12
image: /pages/Pressemitteilungen/2020-08-22-PM_Stadtwerke.jpeg
---

*Pressemitteilung des Augsburger Klimacamps am 22. August 2020*

# Klimagerechtigkeitsaktivist\*innen übernehmen Marketingjob der Stadtwerke

![Klimagerechtigkeit für zu Hause, wechseln Sie zu unserem Tarif 'Power@Home'"](2020-08-22-PM_Stadtwerke.jpeg)

In der Nacht vom Freitag zum Samstag brachten Aktivist\*innen des
Augsburger Klimacamps auf Werbetafeln in der Innenstadt Werbung für den
Tarif „Power@Home“ der Stadtwerke Augsburg an (Foto von der Tafel in der
Schießgrabenstraße zur freien Verwendung angehängt): „Klimagerechtigkeit
für zu Hause, wechseln Sie zu unserem Tarif 'Power@Home'“.

Mit dem Tarif „Power@Home“ können Augsburger Bürger\*innen unabhängig von
der Klimapolitik von Stadt, Land und Bund selbst ihren Teil zu einer
dezentralen Energiewende beitragen. Die Stadtwerke installieren dabei
als Dienstleister eine Solaranlage auf dem Dach und, als
Übergangstechnologie, ein Blockheizkraftwerk im Keller. Ein solches
verbrennt zur Stromproduktion Gas; die entstehende Abwärme kann zur
Gebäudeheizung verwendet werden.

„Der Tarif ist auf lange Sicht günstiger als der Standardtarif und ein
guter Ansatz, um Klimagerechtigkeit auf lokaler Ebene deutlich
voranzubringen“, erklärt Aktivistin Luzia Menacher (19) ihre Motivation.
„Es ist widersprüchlich, dass ein Unternehmen, das klimafreundlicher
werden möchte, ein solches Angebot nicht bewirbt. Der neueste Flyer auf
der Webseite ist von 2013, die angegebenen Telefonnummern nicht mehr
gültig. Unsere Werbeaktion ist keine ernsthafte Vermarktung der
Stadtwerke, aber hoffentlich ein zukunftsorientierter Denkanstoß.“

„Tatsächlich haben wir bei der Aktion aber auch gemischte Gefühle“,
ergänzt der Ravensburger Klimacamper Samuel Bosch (17). „Rund ein
Drittel des Stroms beim Standardtarif der Stadtwerke ist fossilen
Ursprungs, befeuert also die Klimakrise.“ Die Aktivist\*innen des
Klimacamps empfehlen deswegen allen Bürger\*innen Augsburgs, ihren Strom
von Anbietern zu beziehen, die ausschließlich auf erneuerbare Energien
setzen. „Bei diesen Anbietern handelt es sich aber nicht um regionale
Unternehmen, daher rufen wir dazu auf, sich mit einem deutlichen
Wunsch nach klimafreundlicher, günstiger und lokal erzeugter Energie an
die Stadtwerke zu wenden und so auf ihre Ambitionslücke hinzuweisen.“
