---
layout: page
title:  "17.07.2020: Gerichtsurteil: Das Klimacamp durch die Versammlungsfreiheit gedeckt."
date:   2020-07-14 07:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 2
---

*Pressemitteilung vom Augsburger Klimacamp am 17. Juli 2020*

# Gerichtsurteil: Das Klimacamp durch die Versammlungsfreiheit gedeckt.

Heute Vormittag entschied das Verwaltungsgericht zu unserem Gunsten. Das
Klimacamp am Rathaus genießt also weiterhin den Schutz der
Versammlungsfreiheit.

...

In den letzten Tagen analysierten wir die bisher beschlossenen sowie die im
Koalitionsvertrag in Aussicht gestellten Klimaschutzziele Augsburgs von einer
wissenschaftlichen Perspektive. Der bisherige Kurs der Stadtregierung lässt
zwar nicht erkennen, dass diese Ziele eingehalten werden; zu einem Teil liegt
das auch an der Bundesregierung, die nicht geeignete Anreizmöglichkeiten
schafft und mit dem Kohleeinstiegsgesetz plant, gegen das Pariser Klimaabkommen
zu verstoßen.

Trotzdem sind die Ziele von symbolischem Interesse. Sollten sie eingehalten
werden, wird Augsburg noch 34 Millionen Tonnen CO₂ emittieren (siehe Anhang).
Diese Zahl ist in Beziehung zu setzen zu dem CO₂-Budget, das die
Weltgemeinschaft noch emittieren kann, um mit einiger Wahrscheinlichkeit die
Erderhitzung auf das 1,5-°C-Schreckensszenario zu begrenzen. Umgerechnet auf
die Bevölkerung Augsburgs stehen nämlich nur 11 Millionen Tonnen CO₂ zur
Verfügung. Die Stadt plant also, das uns zustehende Budget um den Faktor 3 zu
überschreiten.

Natürlich ist diese Rechnung simpel gestrickt, da unsichere Kippelemente nicht
berücksichtigt sind und willkürlich 2020 als Startdatum gewählt wurde. Bewertet
man die Gesamtemissionen Deutschlands seit 1850, so hat Deutschland das
verfügbare Budget bereits vor langer Zeit vollständig aufgebraucht.

Wir fordern: Die Stadt soll ein CO₂-Budget veröffentlichen, also
angeben, wie viel Tonnen CO₂ sie noch zu emittieren gedenkt. Wenn das
veröffentlichte Budget von der Pariser Zielvorgabe abweichen sollte, soll sie
ausführlich darstellen, mit welchem Recht sie sich eine derartige Verletzung
herausnimmt.
