---
layout: page
title: Pressespiegel
permalink: /pressespiegel/
nav_order: 7
---

# Pressespiegel
Das sagen die Anderen über uns (unvollständig):

## 13.9.
- Augsburger Allgemeine: [Moria und Klimaschutz: In Augsburg gehen die Protestaktionen weiter](https://www.augsburger-allgemeine.de/augsburg/Moria-und-Klimaschutz-In-Augsburg-gehen-die-Protestaktionen-weiter-id58114026.html)

## 11.9.
- Augsburger Allgemeine: [Die Hermanstraße bekommt einen „Pop-up-Radweg" – aber nur für eine Stunde](https://www.augsburger-allgemeine.de/augsburg/Die-Hermanstrasse-bekommt-einen-Pop-up-Radweg-aber-nur-fuer-eine-Stunde-id58103426.html)
- Augsburger Allgemeine: [Die Stadt entfernt das Hochbeet der Klimacamp-Aktivisten an der Maxstraße](https://www.augsburger-allgemeine.de/augsburg/Die-Stadt-entfernt-das-Hochbeet-der-Klimacamp-Aktivisten-an-der-Maxstrasse-id58101506.html)
- hochbeet.stangl.eu:
    - [Hochbeet von Klimaaktivisten auf einem Parkplatz](https://hochbeet.stangl.eu/hochbeet-von-klimaaktivisten-auf-einem-parkplatz/)
    - [Hochbeetentfernung](https://hochbeet.stangl.eu/hochbeetentfernung/)

## 10.9.
- Bayrischer Rundfunk: ["Platz-Park": Augsburger Klimaaktivisten blockieren Parkplatz ](https://www.br.de/nachrichten/bayern/platz-park-augsburger-klimaaktivisten-blockieren-parkplatz,SAAKXVZ)
- Augsburger Allgemeine: [Klimacamp-Aktivisten machen einen Parkplatz in der Maxstraße zum Beet](https://www.augsburger-allgemeine.de/augsburg/Klimacamp-Aktivisten-machen-einen-Parkplatz-in-der-Maxstrasse-zum-Beet-id58094576.html)

## 2.9.
- Augsburger Allgemeine: [Vier Monate Schwarz-Grün in Augsburg: In Teilen der CSU grummelt es](https://augsburger-allgemeine.de/augsburg/Vier-Monate-Schwarz-Gruen-in-Augsburg-In-Teilen-der-CSU-grummelt-es-id58031906.html)

## 28.8.
- DAV Augsburg: [Nachlese Augsburger Klimacamp & aktueller Klimaschutzbericht](https://www.dav-augsburg.de/aav/verein-berichte/1429-nachlese-augsburger-klimacamp-aktueller-klimaschutzbericht)

## 20.8.
- Augsburger Allgemeine: ["Pop-up-Radweg"-Protest: Hermanstraße könnte dauerhafte Radspur bekommen](https://outline.com/cG6c6E)

## 17.8.
- Augsburger Allgemeine: [Klimaschützer wollen den Winter über im Camp am Augsburger Rathaus bleiben](https://augsburger-allgemeine.de/augsburg/Klimaschuetzer-wollen-den-Winter-ueber-im-Camp-am-Augsburger-Rathaus-bleiben-id57939946.html)

## 15.8.
- Forum solidarisches und friedliches Augsburg: [Blockade von Premium Aerotec Werk III in Haunstetten – das etwas andere Friedensfest](http://www.forumaugsburg.de/s_2kommunal/Friedensstadt/200815_blockade-von-premium-aerotec-werk-iii-zum-friedensfest/index.htm)

## 12.8.
- Augsburger Hochschulmagazin presstige: [43 Tage Klimacamp – Ende offen](http://presstige.org/2020/08/43-tage-klimacamp-ende-offen/)
- Augsburger Allgemeine: [Verkehr, Klimaschutz und mehr: Wie nachhaltig ist Augsburg?](https://www.augsburger-allgemeine.de/augsburg/Verkehr-Klimaschutz-und-mehr-Wie-nachhaltig-ist-Augsburg-id57915371.html)

## 8.8.
- Bayrischer Rundfunk: [Klimacamp protestiert zum Friedensfest vor Premium Aerotec](https://www.br.de/nachrichten/bayern/klimacamp-protestiert-zum-friedensfest-vor-premium-aerotec,S73t481)

## 7.8.
- Bayrischer Rundfunk: [Die Augsburger Hermanstraße wird zum Pop-up-Radweg](https://www.br.de/nachrichten/bayern/die-augsburger-hermanstrasse-wird-zum-pop-up-radweg,S6ypNyy)

## 6.8.
- [Demonstrationskultur in Zeiten von Corona: Was macht eigentlich Fridays for Future gerade?](https://www.mucbook.de/demonstrationskultur-in-zeiten-von-corona-was-macht-eigentlich-fridays-for-future-gerade/)

## 2.8.
- Stadtzeitung: [Augsburger Radbegehren hat nötige Unterschriften für Bürgerentscheid erreicht](https://www.stadtzeitung.de/region/augsburg/politik/augsburger-radbegehren-hat-noetige-unterschriften-fuer-buergerentscheid-erreicht-id210100.html) (Yeah!)

## 31.7.
- Augsburger Allgemeine: [Umweltaktivisten ziehen vom Klimacamp in Augsburg aus auf die Straße](https://www.augsburger-allgemeine.de/augsburg/Umweltaktivisten-ziehen-vom-Klimacamp-in-Augsburg-aus-auf-die-Strasse-id57847431.html)

## 28.7.
- Die Augsburger Zeitung: [Zuspruch aus ganz Deutschland – auch Luisa Neubauer besucht das Augsburger Klimacamp](https://www.daz-augsburg.de/zuspruch-aus-ganz-deutschland-auch-luisa-neubauer-besucht-das-augsburger-klimacamp/)

## 22.7.
- Stadtzeitung: [Was will das Augsburger Klimacamp erreichen? Die Forderungen der Umweltaktivisten](https://www.stadtzeitung.de/region/augsburg/politik/will-augsburger-klimacamp-erreichen-forderungen-umweltaktivisten-id209634.html)

## 19.7.
- Forum solidarisches und friedliches Augsburg: [Vortrag auf dem Klimacamp von Tobias Walter](http://www.forumaugsburg.de/s_2kommunal/Kommunalreform/200719_vortrag-auf-dem-klimacamp-dezentrale-energiewende-in-augsburg-jetzt/index.htm)

## 17.7.
- Bayrischer Rundfunk: ["Klima-Camp" Augsburg: Aktivisten rechnen mit Eil-Entscheidung](https://www.br.de/nachrichten/bayern/klima-camp-augsburg-aktivisten-rechnen-mit-eil-entscheidung,S4yG3zh)
- Die Augsburger Zeitung: [Verwaltungsgericht Augsburg: Klimacamp ist vom Versammlungsrecht gedeckt](https://www.daz-augsburg.de/verwaltungsgericht-klimacamp-ist-vom-versammlungsrecht-gedeckt/)
- Bayerisches Verwaltungsgericht Augsburg: [Gericht gibt Eilantrag des „Klima-Camp“ statt](https://www.vgh.bayern.de/media/vgaugsburg/presse/klimacamp-versammlung.pdf)

## 14.7.
- Stadtzeitung: ["Klimacamp" neben dem Rathaus wird vorerst noch nicht geräumt](https://www.stadtzeitung.de/region/augsburg/politik/klimacamp-neben-rathaus-vorerst-noch-geraeumt-id209199.html)
- Augsburg Allgemeine: [Das Klimacamp am Augsburger Rathaus wird vorerst nicht geräumt](https://www.augsburger-allgemeine.de/augsburg/Das-Klimacamp-am-Augsburger-Rathaus-wird-vorerst-nicht-geraeumt-id57725176.html)

## 12.7.
* Oberbürgermeisterin Weber (CSU): [Klimacamp nicht von Versammlungsfreiheit gedeckt](https://www.facebook.com/evaweberaugsburg/photos/a.1065950733447370/4072563629452717/?type=3&source=57) (Wir sind entsetzt)
* Forum solidarisches und friedliches Augsburg: [Fridays for Future errichtet Camp neben dem Rathaus](https://www.forumaugsburg.de/s_6kultur/Unterricht/200712_fridays-for-future-errichtet-camp-neben-dem-rathaus-gegen-kohleausstiegsgesetz/index.htm)

## 11.7.
* Augsburger Allgemeine: [Politiker kritisieren angekündigte Räumung des Klimacamps](https://www.augsburger-allgemeine.de/augsburg/Politiker-kritisieren-angekuendigte-Raeumung-des-Klimacamps-id57713976.html)

## 10.7.
* Augsburger Allgemeine: [Die Stadt will das Klimacamp am Augsburger Rathaus räumen lassen](https://www.augsburger-allgemeine.de/augsburg/Die-Stadt-will-das-Klimacamp-am-Augsburger-Rathaus-raeumen-lassen-id57711651.html)
* Augsburger Allgemeine: [Augsburger Klimacamp: Die Stadt agiert kleinkariert](https://www.augsburger-allgemeine.de/augsburg/Augsburger-Klimacamp-Die-Stadt-agiert-kleinkariert-id57712471.html)

## 7.7.
- Augsburger Allgemeine: [Die Klimaschützer wollen ihr Camp am Rathaus noch lange betreiben](https://www.augsburger-allgemeine.de/augsburg/Die-Klimaschuetzer-wollen-ihr-Camp-am-Rathaus-noch-lange-betreiben-id57689296.html)

## 3.7.
- Augsburger Allgemeine: ["Fridays for Future": Klimaschützer melden sich mit Protesten zurück](https://www.augsburger-allgemeine.de/augsburg/Fridays-for-Future-Klimaschuetzer-melden-sich-mit-Protesten-zurueck-id57671656.html)

## 2.7.
- Bayrischer Rundfunk: [Klimaaktivisten besetzen Augsburger Rathaus](https://www.br.de/nachrichten/bayern/klimaaktivisten-besetzen-augsburger-rathaus,S3eOSKj)
