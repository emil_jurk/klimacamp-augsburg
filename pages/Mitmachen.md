---
layout: page
title: Mitmachen
permalink: /mitmachen/
nav_order: 5
---
# Vorbeikommen & mitmachen
## Adresse
Wenn du Lust hast vorbeizukommen, ob für ein kurzes Gespräch oder mehrere Tage - du findest uns direkt am Rathausplatz:

    Am Klimacamp 1
    86150 Augsburg
  
## Packliste
    Trotz Kohleeinstiegsgesetz gute Laune
    Isomatte und Schlafsack (wir bringen auch welche zum Ausleihen mit, können aber wegen Corona nicht wild durchtauschen – wenn du auch Isomatten und Schlafsäcke für andere mitbringen kannst, mach das bitte!)
    Gesichtsmaske
    Essen und Trinken (wir können aber auch das öffentliche Bad/WC im Rathaus nutzen und direkt nebenan ist ein Supermarkt – groß gemeinsam Kochen geht wegen Corona leider nicht)
    Zahnputzzeugs & Co.
    genügend Kleidung für mehrere Tage
    Spiele, Bücher, Musikinstrumente, Boxen, …
    falls zur Hand: Banner/Demoschilder, Strandmuschel, Zelt (falls ohne Heringe aufbaubar), lange Stromkabel
    Wärmflaschen (wir befüllen die mit heißem Wasser für die Nacht – bitte auch mitbringen, falls dir nie kalt ist oder du nicht über Nacht bleibst, zum Ausleihen für andere)
    lange Unterhose o.Ä. für die Nacht
    falls zufälligerweise einfach transportabel: Teppich/Plane, Pavillon, Camping-Stuhl, Sofa, Tisch, warme Decke (für einen selbst oder zum Ausleihen)
    etwas Geld zum Einkaufen (wir sind aber solidarisch und teilen ggf. Kosten auf)
    ggf. Sonnenmilch
    ggf. geladene Powerbank
    ggf. geladene Laptops für Programmierworkshops
    ausgedruckte Unterschriftenlisten für den Radentscheid, damit wir Unterschriften sammeln können (wir bringen auch welche mit)
    Freund*innen!
# kummerkasten  
