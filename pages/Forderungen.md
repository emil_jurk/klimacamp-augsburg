---
layout: page
title: Forderungen
permalink: /forderungen/
nav_order: 3
date: 2020-08-16T13:18:18.107Z
---
# Forderungen
 

## 1) Die Einhaltung des CO₂-Budgets
Die Stadt soll ihren Anteil am 1,5°C-Ziel einhalten
und ihr noch übriges CO₂-Budget öffentlich kommunizieren.
Es soll transparent ein Plan offengelegt werden,
wie viel CO₂ die Stadt noch zu emittieren gedenkt.
Wenn der geplante Ausstoß über dem Anteil liegt,
der der Stadt zusteht, muss begründet werden,
mit welchem Recht die Stadt sich das herausnimmt.
(Das CO₂-Budget für eine Stadt kann sehr einfach ausgerechnet werden.)


## 2) Sofortiger Start der Energiewende in Augsburg   
### → Augsburger Kohleausstieg bis 2025   
### → schneller Solarenergieausbau   


## 3) Öffentliche Positionierung gegen das Kohlegesetz
Wesentliche Kritikpunkte zum am 03.07. beschlossenen KohleEINstiegsgesetz kurz dargestellt
auf der [Internetseite der Scientists for Future](https://www.scientists4future.org/defizite-kohleausstiegsgesetz-kvbg-e/).


## 4) Fahrradstadt JETZT!
Die Stadt Augsburg soll die Verkehrswende voranbringen.
Dazu ist es nötig, das [lokale Radbegehren](https://www.fahrradstadt-jetzt.de/) umzusetzen.
